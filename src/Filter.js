import React, { Component, PropTypes } from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'

class Filter extends Component {
  static propTypes = {
    filter: PropTypes.string.isRequired,
    isActive: PropTypes.bool.isRequired,
    onFilter: PropTypes.func.isRequired
  }

  _handlePress = () => this.props.onFilter(this.props.filter)

  render () {
    const { filter, isActive } = this.props
    return (
      <TouchableOpacity
        style={[styles.main, isActive && styles.selected]}
        onPress={this._handlePress}
      >
        <Text>{filter}</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  main: {
    padding: 8,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'transparent'
  },
  selected: {
    borderColor: 'rgba(175, 47, 47, 0.2)'
  }
})

export default Filter

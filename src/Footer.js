import React, { Component, PropTypes } from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

import Filter from './Filter'

class Footer extends Component {
  static propTypes = {
    itemCount: PropTypes.number,
    filters: PropTypes.arrayOf(PropTypes.string),
    currentFilter: PropTypes.string.isRequired,
    onFilter: PropTypes.func.isRequired,
    onRemoveItemsClick: PropTypes.func.isRequired
  }

  render () {
    const { itemCount, filters, currentFilter, onFilter, onRemoveItemsClick } = this.props

    return (
      <View style={styles.main}>
        <Text>{'Items: '}{itemCount}</Text>
        <View style={styles.filters}>
          {filters.map((filter, i) => (
            <Filter
              key={i}
              filter={filter}
              isActive={filter === currentFilter}
              onFilter={onFilter}
            />
          ))}
        </View>
        <TouchableOpacity onPress={onRemoveItemsClick}>
          <Text>{'X'}</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main: {
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  filters: {
    flexDirection: 'row'
  }
})

export default Footer

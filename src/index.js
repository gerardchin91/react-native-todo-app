import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  ListView,
  Keyboard,
  Alert,
  AsyncStorage,
  ActivityIndicator
} from 'react-native'

import Header from './Header'
import Footer from './Footer'
import Row from './Row'

class App extends Component {
  constructor (props) {
    super(props)
    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
    this.state = {
      filter: 'ALL',
      loading: true,
      allComplete: false,
      value: '',
      items: [],
      dataSource: ds.cloneWithRows([])
    }
  }

  componentWillMount () {
    AsyncStorage.getItem('items')
    .then(json => {
      try {
        const items = JSON.parse(json)
        this._setListSource(items, items, { loading: false })
      } catch (err) {
        this.setState({ loading: false })
      }
    })
  }

  componentDidUpdate (prevProps, prevState) {
    /* --- DEBUGGING --- */
    AsyncStorage.getItem('items')
    .then(json => {
      try {
        console.table(JSON.parse(json))
      } catch (err) {
        console.error(`No items found: ${err}`)
      }
    })
  }

  _handleAddItem = () => {
    if (!this.state.value) return
    const newItems = [
      ...this.state.items,
      { key: Date.now(), text: this.state.value, completed: false }
    ]
    this._setListSource(newItems, filterItems(this.state.filter, newItems), { value: '' })
    this.setState({ items: newItems, value: '' })
  }

  _handleToggleAllComplete = () => {
    const completed = !this.state.allComplete
    const newItems = this.state.items.map(item => ({ ...item, completed }))

    this._setListSource(newItems, filterItems(this.state.filter, newItems), { allComplete: completed })
    this.setState({ items: newItems, allComplete: completed })
  }

  _handleToggleComplete = (key, completed) => {
    const newItems = this.state.items.map(item => {
      if (item.key !== key) return item
      return { ...item, completed }
    })
    this._setListSource(newItems, filterItems(this.state.filter, newItems))
  }

  _handleRemoveItem = key => {
    const newItems = this.state.items.filter(item => item.key !== key)
    this._setListSource(newItems, filterItems(this.state.filter, newItems))
  }

  _handleRemoveCompletedItems = () => {
    const newItems = filterItems('ACTIVE', this.state.items)
    this._setListSource(newItems, filterItems(this.state.filter, newItems))
  }

  _handleFilterChange = filter => {
    this._setListSource(this.state.items, filterItems(filter, this.state.items), { filter })
  }

  _handleUpdateItemText = (key, text) => {
    const newItems = this.state.items.map(item => {
      if (item.key !== key) return item
      return { ...item, text }
    })
    this._setListSource(newItems, filterItems(this.state.filter, newItems))
  }

  _handleToggleEditing = (key, editing) => {
    const newItems = this.state.items.map(item => {
      if (item.key !== key) return item
      return { ...item, editing }
    })
    this._setListSource(newItems, filterItems(this.state.filter, newItems))
  }

  _toggleRemoveCompletedAlert = () => {
    Alert.alert(
      'Warning:',
      'Are you sure you want to remove all completed items?',
      [
        { text: 'Cancel', style: 'cancel' },
        { text: 'Yes', onPress: this._handleRemoveCompletedItems }
      ]
    )
  }

  _setListSource = (items, itemsDataSource, otherState = {}) => {
    this.setState({
      items,
      dataSource: this.state.dataSource.cloneWithRows(itemsDataSource),
      ...otherState
    })
    AsyncStorage.setItem('items', JSON.stringify(items))
  }

  _setInputValue = value => this.setState({ value })

  _dismissKeyboard = () => Keyboard.dismiss()

  _renderRow = ({ key, ...value }) => {
    return (
      <Row
        key={key}
        {...value}
        onUpdate={text => this._handleUpdateItemText(key, text)}
        onToggleEdit={editing => this._handleToggleEditing(key, editing)}
        onComplete={completed => this._handleToggleComplete(key, completed)}
        onDestroy={() => this._handleRemoveItem(key)}
      />
    )
  }

  _renderSeparator = (sectionId, rowId) => (<View key={rowId} style={styles.separator} />)

  render () {
    const filters = ['ALL', 'ACTIVE', 'COMPLETED']

    return (
      <View style={styles.container}>
        <Header
          value={this.state.value}
          onAddItem={this._handleAddItem}
          onChange={this._setInputValue}
          onToggleAllComplete={this._handleToggleAllComplete}
        />
        <View style={styles.content}>
          <ListView
            style={styles.list}
            enableEmptySections
            dataSource={this.state.dataSource}
            onScroll={this._dismissKeyboard}
            renderRow={this._renderRow}
            renderSeparator={this._renderSeparator}
          />
        </View>
        <Footer
          itemCount={filterItems(this.state.filter, this.state.items).length}
          filters={filters}
          currentFilter={this.state.filter}
          onFilter={this._handleFilterChange}
          onRemoveItemsClick={this._toggleRemoveCompletedAlert}
        />
        {this.state.loading &&
          <View style={styles.loading}>
            <ActivityIndicator animating size={'large'} />
          </View>
        }
      </View>
    )
  }
}

const filterItems = (filter, items) => {
  return items.filter(item => {
    switch (filter) {
      case 'ALL':
        return true
      case 'COMPLETED':
        return item.completed
      case 'ACTIVE':
        return !item.completed
      default:
        return true
    }
  })
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5'
  },
  content: {
    flex: 1
  },
  list: {
    backgroundColor: '#fff'
  },
  separator: {
    borderWidth: 1,
    borderColor: '#ddd'
  },
  loading: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.2)'
  }
})

export default App

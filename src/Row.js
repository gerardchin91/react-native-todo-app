import React, { Component, PropTypes } from 'react'
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  Switch,
  TouchableOpacity
} from 'react-native'

class Row extends Component {
  static propTypes = {
    editing: PropTypes.bool,
    text: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired,
    onComplete: PropTypes.func.isRequired,
    onDestroy: PropTypes.func.isRequired,
    onUpdate: PropTypes.func.isRequired,
    onToggleEdit: PropTypes.func.isRequired
  }

  _enterEditMode = () => this.props.onToggleEdit(true)

  _leaveEditMode = () => this.props.onToggleEdit(false)

  render () {
    const { editing, text, completed, onComplete, onDestroy, onUpdate } = this.props

    const textComponent = (
      <TouchableOpacity style={styles.textWrap} onLongPress={this._enterEditMode}>
        <Text style={[styles.text, completed && styles.completed]}>{text}</Text>
      </TouchableOpacity>
    )

    const removeButton = (
      <TouchableOpacity onPress={onDestroy}>
        <Text style={styles.destroy}>{'X'}</Text>
      </TouchableOpacity>
    )

    const doneButton = (
      <TouchableOpacity onPress={this._leaveEditMode}>
        <Text style={styles.save}>{String.fromCharCode(10003)}</Text>
      </TouchableOpacity>
    )

    const editingComponent = (
      <View style={styles.textWrap}>
        <TextInput
          autoFocus
          multiline
          value={text}
          style={styles.input}
          onChangeText={onUpdate}
        />
      </View>
    )

    return (
      <View style={styles.main}>
        <Switch
          value={completed}
          onValueChange={onComplete}
        />
        {editing ? editingComponent : textComponent}
        {editing ? doneButton : removeButton }
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between'
  },
  input: {
    height: 100,
    flex: 1,
    fontSize: 24,
    padding: 0,
    color: '#4d4d4d'
  },
  textWrap: {
    flex: 1,
    marginHorizontal: 10
  },
  text: {
    fontSize: 24,
    color: '#4d4d4d'
  },
  completed: {
    textDecorationLine: 'line-through'
  },
  destroy: {
    fontSize: 20,
    color: '#cc9a9a'
  },
  save: {
    fontSize: 20,
    color: '#1abc9c'
  }
})

export default Row

import React, { Component, PropTypes } from 'react'
import { View, Text, StyleSheet, TextInput, Platform, TouchableOpacity } from 'react-native'

class Header extends Component {
  static propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    onAddItem: PropTypes.func.isRequired,
    onToggleAllComplete: PropTypes.func.isRequired
  }

  render () {
    const { value, onChange, onAddItem, onToggleAllComplete } = this.props

    return (
      <View style={styles.main}>
        <TouchableOpacity onPress={onToggleAllComplete}>
          <Text style={styles.toggleIcon}>
            {String.fromCharCode(10003)}
          </Text>
        </TouchableOpacity>
        <TextInput
          value={value}
          onChangeText={onChange}
          onSubmitEditing={onAddItem}
          style={styles.input}
          placeholder={'What needs to be done?'}
          blurOnSubmit={false}
          returnKeyType={'done'}
          placeholderTextColor={'#ddd'}
          underlineColorAndroid={'#ddd'}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: '#16a085',
    paddingHorizontal: 16,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    ...Platform.select({
      ios: {
        paddingTop: 30
      }
    })
  },
  toggleIcon: {
    fontSize: 30,
    color: '#ddd'
  },
  input: {
    flex: 1,
    height: 50,
    marginLeft: 60
  }
})

export default Header
